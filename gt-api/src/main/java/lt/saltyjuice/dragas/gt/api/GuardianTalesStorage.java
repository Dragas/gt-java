package lt.saltyjuice.dragas.gt.api;

import java.util.Set;

public interface GuardianTalesStorage {
    void storeAccountNumber(AccountNumber account) throws GuardianTalesException;
    Set<AccountNumber> getAccounts();
}
