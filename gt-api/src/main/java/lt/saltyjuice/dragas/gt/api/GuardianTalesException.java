package lt.saltyjuice.dragas.gt.api;

public class GuardianTalesException extends Exception {
    public GuardianTalesException() {
        super();
    }

    public GuardianTalesException(String message) {
        super(message);
    }

    public GuardianTalesException(String message, Throwable cause) {
        super(message, cause);
    }

    public GuardianTalesException(Throwable cause) {
        super(cause);
    }

    protected GuardianTalesException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
