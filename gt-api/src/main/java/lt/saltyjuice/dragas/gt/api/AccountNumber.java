package lt.saltyjuice.dragas.gt.api;

import java.time.OffsetDateTime;
import java.util.Comparator;

public class AccountNumber implements Comparable<AccountNumber> {
    private String name;
    private String accountNumber;
    private OffsetDateTime subscribedAt;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public OffsetDateTime getSubscribedAt() {
        return subscribedAt;
    }

    public void setSubscribedAt(OffsetDateTime subscribedAt) {
        this.subscribedAt = subscribedAt;
    }

    @Override
    public int compareTo(AccountNumber o) {
        return getAccountNumber().compareTo(o.getAccountNumber());
    }
}
