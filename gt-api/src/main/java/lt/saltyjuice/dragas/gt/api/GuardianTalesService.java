package lt.saltyjuice.dragas.gt.api;

import java.util.List;

public interface GuardianTalesService {
    boolean subscribe(String username, String accountNumber) throws GuardianTalesException;
    boolean claimCode(String accountNumber, String region, String code);
    List<String> claimCodes(String code);

    int getAccountNumberCount();
}
