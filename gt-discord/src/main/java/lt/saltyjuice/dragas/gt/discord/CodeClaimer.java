package lt.saltyjuice.dragas.gt.discord;

import lt.saltyjuice.dragas.gt.api.GuardianTalesService;
import net.dv8tion.jda.api.events.interaction.command.SlashCommandInteractionEvent;
import net.dv8tion.jda.api.interactions.commands.OptionMapping;

import java.util.List;
import java.util.stream.Collectors;

public class CodeClaimer extends CommandListener {
    private final GuardianTalesService service;

    public CodeClaimer(GuardianTalesService service) {
        super("claim");
        this.service = service;
    }

    @Override
    protected void onSlashCommand(SlashCommandInteractionEvent event) {
        event.deferReply(true).queue();
        String code = event.getOption("code", OptionMapping::getAsString);
        List<String> responses = service.claimCodes(code);
        if (responses.isEmpty()) {
            int accNumberCount = service.getAccountNumberCount();
            event.getHook().editOriginal(String.format("Successfully claimed code for %s users", accNumberCount)).queue();
        } else {
            event.getHook().editOriginal(responses.stream().collect(Collectors.joining(System.lineSeparator()))).queue();
        }
    }
}
