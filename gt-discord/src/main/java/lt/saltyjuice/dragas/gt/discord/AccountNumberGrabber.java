package lt.saltyjuice.dragas.gt.discord;

import lt.saltyjuice.dragas.gt.api.GuardianTalesException;
import lt.saltyjuice.dragas.gt.api.GuardianTalesService;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.events.interaction.command.SlashCommandInteractionEvent;
import net.dv8tion.jda.api.events.session.ReadyEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import net.dv8tion.jda.api.interactions.commands.OptionMapping;
import net.dv8tion.jda.api.interactions.commands.OptionType;
import net.dv8tion.jda.api.interactions.commands.build.Commands;

public class AccountNumberGrabber extends CommandListener {
    private final GuardianTalesService service;

    public AccountNumberGrabber(GuardianTalesService service) {
        super("subscribe");
        this.service = service;
    }

    @Override
    protected void onSlashCommand(SlashCommandInteractionEvent event) {
        String accountNumber = event.getOption("accnumber", OptionMapping::getAsString);
        event.deferReply(true).queue();
        try {
            service.subscribe(event.getUser().getName(), accountNumber);
            event.getHook().editOriginal("Okay you're subbed").queue();
        } catch (GuardianTalesException e) {
            event.getHook().editOriginal(String.format("Failed to subscribe: %s", e.getMessage())).queue();
        }
    }
}
