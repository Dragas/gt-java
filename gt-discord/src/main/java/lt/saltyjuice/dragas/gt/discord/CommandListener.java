package lt.saltyjuice.dragas.gt.discord;

import net.dv8tion.jda.api.events.interaction.command.SlashCommandInteractionEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;

public abstract class CommandListener extends ListenerAdapter {
    private final String command;

    public CommandListener(String command) {
        this.command = command;
    }

    @Override
    public final void onSlashCommandInteraction(SlashCommandInteractionEvent event) {
        super.onSlashCommandInteraction(event);
        String name = event.getName();
        if(command.equals(name)) {
            onSlashCommand(event);
        }
    }

    protected abstract void onSlashCommand(SlashCommandInteractionEvent event);
}
