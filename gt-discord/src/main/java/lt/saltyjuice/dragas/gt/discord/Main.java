package lt.saltyjuice.dragas.gt.discord;

import lt.saltyjuice.dragas.gt.api.GuardianTalesService;
import lt.saltyjuice.dragas.gt.api.GuardianTalesStorage;
import lt.saltyjuice.dragas.gt.core.FileGuardianTalesStorage;
import lt.saltyjuice.dragas.gt.retrofit.RetrofitGuardianTalesService;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.JDABuilder;
import net.dv8tion.jda.api.Permission;
import net.dv8tion.jda.api.interactions.commands.DefaultMemberPermissions;
import net.dv8tion.jda.api.interactions.commands.OptionType;
import net.dv8tion.jda.api.interactions.commands.build.Commands;

import java.io.IOException;
import java.time.Clock;
import java.util.Arrays;

public class Main {
    public static void main(String[] args) throws IOException {
        GuardianTalesStorage storage = new FileGuardianTalesStorage("gt.txt");
        GuardianTalesService service = new RetrofitGuardianTalesService(storage, Clock.systemUTC());
        JDA jda = JDABuilder.create(args[0], Arrays.asList())
                .addEventListeners(new AccountNumberGrabber(service))
                .addEventListeners(new CodeClaimer(service))
                .build();
        jda.updateCommands()
                .addCommands(
                        Commands.slash("claim", "Claims guardian tales code for everyone subbed")
                                .addOption(OptionType.STRING, "code", "code to claim for everyone", true)
                                .setDefaultPermissions(DefaultMemberPermissions.DISABLED),
                        Commands.slash("subscribe", "Subscribes you to getting freebies in gt")
                                .addOption(OptionType.STRING, "accnumber", "your account number", true)
                ).queue();
    }
}
