package lt.saltyjuice.dragas.gt.core;

import lt.saltyjuice.dragas.gt.api.AccountNumber;
import lt.saltyjuice.dragas.gt.api.GuardianTalesException;
import lt.saltyjuice.dragas.gt.api.GuardianTalesStorage;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;
import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Collections;
import java.util.Scanner;
import java.util.Set;
import java.util.TreeSet;

public class FileGuardianTalesStorage implements GuardianTalesStorage {
    private static final DateTimeFormatter DTF = DateTimeFormatter.ISO_OFFSET_DATE_TIME;
    private final String storageName;
    private final File file;
    private final Set<AccountNumber> accounts;

    public FileGuardianTalesStorage(String filename) throws IOException {
        this.accounts = Collections.synchronizedSet(new TreeSet<>());
        this.storageName = filename;
        this.file = new File(filename);
        if(!this.file.exists()) {
            this.file.createNewFile();
        }
        try(Scanner sin = new Scanner(this.file)) {
            while(sin.hasNextLine()) {
                String line = sin.nextLine();
                String[] split = line.split(" ");
                AccountNumber number = new AccountNumber();
                number.setName(split[0]);
                number.setAccountNumber(split[1]);
                OffsetDateTime subscribedAt = OffsetDateTime.parse(split[2], DTF);
                number.setSubscribedAt(subscribedAt);
                this.accounts.add(number);
            }
        };
    }

    @Override
    public synchronized void storeAccountNumber(AccountNumber account) throws GuardianTalesException {
        try (FileOutputStream fout = new FileOutputStream(this.file, true)) {
            boolean addedAccount = accounts.add(account);
            if(!addedAccount) {
                throw new GuardianTalesException("Duplicate account number");
            }
            PrintWriter pw = new PrintWriter(fout, true, StandardCharsets.UTF_8);
            pw.printf("%s %s %s%n", account.getName(), account.getAccountNumber(), DTF.format(account.getSubscribedAt()));
        }
        catch(IOException e) {
            throw new GuardianTalesException("Failed to store reference", e);
        };
    }

    @Override
    public Set<AccountNumber> getAccounts() {
        Set<AccountNumber> copy = new TreeSet<>(this.accounts);
        return copy;
    }
}
