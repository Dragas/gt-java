package lt.saltyjuice.dragas.gt.retrofit;

import lt.saltyjuice.dragas.gt.api.AccountNumber;
import lt.saltyjuice.dragas.gt.api.GuardianTalesException;
import lt.saltyjuice.dragas.gt.api.GuardianTalesService;
import lt.saltyjuice.dragas.gt.api.GuardianTalesStorage;
import okhttp3.OkHttpClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.scalars.ScalarsConverterFactory;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.time.Clock;
import java.time.Duration;
import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class RetrofitGuardianTalesService implements GuardianTalesService {

    private final GuardianTalesStorage storage;
    private final Clock clock;
    private final GuardianTalesWebsiteApi website;
    private static final Logger LOG = LoggerFactory.getLogger(RetrofitGuardianTalesService.class);

    public RetrofitGuardianTalesService(GuardianTalesStorage storage, Clock clock) {
        this.clock = clock;
        this.storage = storage;
        OkHttpClient okHttpClient = new OkHttpClient.Builder()
                //.proxy(new Proxy(Proxy.Type.HTTP, new InetSocketAddress("localhost", 8080)))
                .callTimeout(Duration.ofMinutes(1))
                .readTimeout(Duration.ofMinutes(1))
                .writeTimeout(Duration.ofMinutes(1))
                .connectTimeout(Duration.ofMinutes(1))
                .build();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://guardiantales.com/")
                .validateEagerly(true)
                .client(okHttpClient)
                .addConverterFactory(ScalarsConverterFactory.create())
                .build();
        website = retrofit.create(GuardianTalesWebsiteApi.class);
    }
    @Override
    public boolean subscribe(String username, String accountNumber) throws GuardianTalesException {
        OffsetDateTime now = OffsetDateTime.now(clock);
        AccountNumber acc = new AccountNumber();
        acc.setName(username);
        acc.setAccountNumber(accountNumber);
        acc.setSubscribedAt(now);
        storage.storeAccountNumber(acc);
        return true;
    }

    @Override
    public boolean claimCode(String accountNumber, String region, String code) {
        try {
            Response<String> response = website.claim(accountNumber, region, code).execute();
            if(!response.isSuccessful()) {
                throw new IOException(String.format("Failed to claim code. Error response: %s", response.code()));
            }
            return true;
        }
        catch(IOException e) {
            LOG.error(String.format("Failed to claim code for %s", accountNumber), e);
        }
        return false;
    }

    @Override
    public synchronized List<String> claimCodes(String code) {
        Set<AccountNumber> accountNumbers = storage.getAccounts();
        List<String> errors = new ArrayList<>();
        for (AccountNumber accountNumber : accountNumbers) {
            boolean claimed = claimCode(accountNumber.getAccountNumber(), "EU", code);
            if(!claimed) {
                errors.add(String.format("%s (%s) had already claimed the code.", accountNumber.getName(), accountNumber.getAccountNumber()));
            }
        }
        return errors;
    }

    @Override
    public int getAccountNumberCount() {
        return storage.getAccounts().size();
    }
}
