package lt.saltyjuice.dragas.gt.retrofit;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;
import retrofit2.http.Url;

public interface GuardianTalesWebsiteApi {
    @FormUrlEncoded
    @POST("coupon/redeem")
    Call<String> claim(@Field("userId") String userId, @Field("region") String region, @Field("code") String code);
}
